
try:
    import pymongo
    from pymongo import MongoClient
    import pandas as pd

except Exception as exception:
    print("Please install missing modules")


myClient = MongoClient("localhost", 27017)
myDB = myClient["test"]
myCollection = myDB["test"]


def addData(path=None):
    data = pd.read_csv(path)
    data_ = data.to_dict('records')

    myCollection.insert_many(data_, ordered=False)
    print("All the Data has been Exported to Mongo DB Server .... ")

addData(path="csiro_alt_gmsl_mo_2015_csv.csv")



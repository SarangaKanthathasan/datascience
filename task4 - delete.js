var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017';

MongoClient.connect(url,function(err,db){
    if(err)throw err;
    var mydb = db.db('test');
    var query = {Time : '7/15/1993'};

    mydb.collection('test').deleteOne(query, function(err,obj){
        if(err) throw err;
        console.log('Data is deleted.')
        db.close();
    });
}); 
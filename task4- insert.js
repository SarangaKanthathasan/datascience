var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017';

MongoClient.connect(url,function(err,db){
    if(err)throw err;
    var mydb = db.db('test');
    var mydata = {Time : '01/02/1999',GMSL : '1.22'};

    mydb.collection('test').insertOne(mydata, function(err,res){
        if(err) throw err;
        console.log('Data is inserted.')
        db.close();
    });
});
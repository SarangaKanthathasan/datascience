
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

data = pd.read_csv("task10.csv")

X = data.iloc[:, 0:1].values
Y = data.iloc[:, 1].values

linear_regressor = LinearRegression()
linear_regressor.fit(X, Y)

def show_linear():
    plt.scatter(X, Y, color = "blue")
    plt.plot(X, linear_regressor.predict(X), color='red')
    plt.title("Linear Regression")
    plt.xlabel("year")
    plt.ylabel("temperature annomaly")
    plt.show()

show_linear()

poly_reg = PolynomialFeatures(degree=4)
x_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(x_poly,Y)

def show_poly():
    plt.scatter(X,Y, color = "red")
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)),color = "blue")
    plt.title("Polynomial Regression")
    plt.xlabel("year")
    plt.ylabel("temperature annomaly")
    plt.show()

show_poly()

print("year\t linear \t polynomial")
for i in range(1,31):
    print(2020+i,"\t", format(linear_regressor.predict([[2020+i]])[0],".4f"),"\t",format(pol_reg.predict(poly_reg.fit_transform([[2020+i]]))[0],".4f"))


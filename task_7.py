import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
from sklearn.cluster import KMeans

data = pd.read_csv("csiro_alt_gmsl_mo_2015_csv.csv")
x=data.iloc[:,[1]]
print(x)

plt.scatter(data['Time'],data['GMSL'])
plt.show()

kmeans = KMeans(3)
kmeans.fit(x)
identified_clusters = kmeans.fit_predict(x)
data_cluster = data.copy()
data_cluster['cluster'] = identified_clusters

plt.scatter(data_cluster['Time'],data_cluster['GMSL'],c=data_cluster['cluster'])
plt.title("K means Clusters")
plt.show()



from sklearn import tree
import pandas as pd
import pydotplus
from IPython.display import display, Image


data_annual = pd.read_csv("csiro_alt_gmsl_mo_2015_csv - Copy.csv")
duplicate_data = data_annual.iloc[:, 0:3].values

data_annual = data_annual[['Time','GMSL','level']]
print(data_annual)
one_data = pd.get_dummies(data_annual)
print(one_data)

clf = tree.DecisionTreeClassifier()
clf_train = clf.fit(one_data, data_annual['level'])

dot_data = tree.export_graphviz(clf_train, out_file=None, feature_names=list(one_data.columns.values),
                                class_names=['low', 'medium', 'high'], rounded=True, filled=True)
graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())


